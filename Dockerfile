# Pull base image
FROM nepalez/ruby

MAINTAINER Kristijan Rebernisak <kristijan@shoutem.com>
ENV NODE_VERSION "6.2.0"

# Install packages
RUN apt-get update && apt-get install -y git nodejs-legacy npm wget

RUN wget -O api-console.tar.gz https://github.com/mulesoft/api-console/archive/v3.0.7.tar.gz
RUN tar -xvzf api-console.tar.gz
RUN rm api-console.tar.gz

RUN git clone git://github.com/OiNutter/nodenv.git /root/.nodenv && \
git clone git://github.com/OiNutter/node-build.git /root/.nodenv/plugins/node-build
ENV PATH /root/.nodenv/shims:/root/.nodenv/bin:/root/.nodenv/versions/$NODE_VERSION/bin:$PATH

# INSTALL NODENV VERSION
RUN nodenv install "$NODE_VERSION"
RUN nodenv global "$NODE_VERSION"
RUN nodenv rehash


WORKDIR api-console-3.0.7
# Install SASS, Bower & Grunt
RUN gem install sass
RUN npm install --silent -g bower grunt-cli

RUN npm install --silent
RUN cd /app/api-console-3.0.7 && bower install --allow-root

# Exposing port 9000 because this is the port api-console server uses
EXPOSE 9000

CMD [ "grunt", "--force"]
